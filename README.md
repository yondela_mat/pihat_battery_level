# PiHat Battery Level

This project is meant to make a microHat that will be able to tell the Pi Zero
the level of the battery when it is running using a battery instead of being
powered using electricity from the mains.

## How to use the PiHat

1. You will need to connect it to the Pi Zero and ensure that all the 40-GPIO
pins are connected properly.
2. You will then need to connect the PiHat to a battery source that is 9 volts
so that it can start operating.
